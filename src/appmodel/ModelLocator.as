package appmodel
{
	import mx.collections.ArrayCollection;
	
	import services.SignLineUpdaterService;

	[Bindable]
	public class ModelLocator {
		
		public const NS_API_CALL_DELAY:Number = 20 * 1000;
		
		public var signLineUpdaterService:SignLineUpdaterService;
		
		public var minuteReel:ArrayCollection = new ArrayCollection([""]);
		public var hourReel:ArrayCollection = new ArrayCollection([""]);
		public var trackReel:ArrayCollection = new ArrayCollection([""]);
		public var destinationReel:ArrayCollection = new ArrayCollection([""]);
		public var delayReel:ArrayCollection = new ArrayCollection([""]);
		public var trainTypeReel:ArrayCollection = new ArrayCollection([""]);
		
		public var signLines:Array = new Array();
		public var oldSignLines:Array = new Array(); // used when resizing
		public var signUpdateQueue:Array = new Array();
		
		//Required for the model to function
		private static var instance:ModelLocator; 
		public function ModelLocator(enforcer:SingletonEnforcer){
			if (enforcer == null) {
				throw new Error( "You Can Only Have One ModelLocator" );
			}
			initReels();
		}
		
		public static function getInstance() : ModelLocator {
			if (instance == null) {
				instance = new ModelLocator( new SingletonEnforcer );
			}
			return instance;
		}
		
		private function initReels():void {
			var i:int;
			var value:String;
			for (i = 0 ; i < 60 ; i++) {
				value = i < 10 ? "0"+i : ""+i;
				minuteReel.addItem(value);
			}
			for (i = 0 ; i < 24 ; i++) {
				value = i < 10 ? "0"+i : ""+i;
				hourReel.addItem(value);
			}		
		}
	}
}
class SingletonEnforcer {}