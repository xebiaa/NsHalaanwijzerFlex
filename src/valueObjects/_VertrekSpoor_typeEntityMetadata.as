
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.styles.StyleValidator;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.events.ValidationResultEvent;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _VertrekSpoor_typeEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("wijziging", "VertrekSpoor");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array("wijziging", "VertrekSpoor");
    model_internal static var allAlwaysAvailableProperties:Array = new Array("wijziging", "VertrekSpoor");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("wijziging", "VertrekSpoor");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("wijziging", "VertrekSpoor");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "VertrekSpoor_type";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;

    
    model_internal var _wijzigingIsValid:Boolean;
    model_internal var _wijzigingValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _wijzigingIsValidCacheInitialized:Boolean = false;
    model_internal var _wijzigingValidationFailureMessages:Array;
    
    model_internal var _VertrekSpoorIsValid:Boolean;
    model_internal var _VertrekSpoorValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _VertrekSpoorIsValidCacheInitialized:Boolean = false;
    model_internal var _VertrekSpoorValidationFailureMessages:Array;

    model_internal var _instance:_Super_VertrekSpoor_type;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _VertrekSpoor_typeEntityMetadata(value : _Super_VertrekSpoor_type)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["wijziging"] = new Array();
            model_internal::dependentsOnMap["VertrekSpoor"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["wijziging"] = "String";
        model_internal::propertyTypeMap["VertrekSpoor"] = "String";

        model_internal::_instance = value;
        model_internal::_wijzigingValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForWijziging);
        model_internal::_wijzigingValidator.required = true;
        model_internal::_wijzigingValidator.requiredFieldError = "wijziging is required";
        //model_internal::_wijzigingValidator.source = model_internal::_instance;
        //model_internal::_wijzigingValidator.property = "wijziging";
        model_internal::_VertrekSpoorValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForVertrekSpoor);
        model_internal::_VertrekSpoorValidator.required = true;
        model_internal::_VertrekSpoorValidator.requiredFieldError = "VertrekSpoor is required";
        //model_internal::_VertrekSpoorValidator.source = model_internal::_instance;
        //model_internal::_VertrekSpoorValidator.property = "VertrekSpoor";
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity VertrekSpoor_type");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity VertrekSpoor_type");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of VertrekSpoor_type");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity VertrekSpoor_type");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity VertrekSpoor_type");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity VertrekSpoor_type");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isWijzigingAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isVertrekSpoorAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */
    public function invalidateDependentOnWijziging():void
    {
        if (model_internal::_wijzigingIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfWijziging = null;
            model_internal::calculateWijzigingIsValid();
        }
    }
    public function invalidateDependentOnVertrekSpoor():void
    {
        if (model_internal::_VertrekSpoorIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfVertrekSpoor = null;
            model_internal::calculateVertrekSpoorIsValid();
        }
    }

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get wijzigingStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get wijzigingValidator() : StyleValidator
    {
        return model_internal::_wijzigingValidator;
    }

    model_internal function set _wijzigingIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_wijzigingIsValid;         
        if (oldValue !== value)
        {
            model_internal::_wijzigingIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "wijzigingIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get wijzigingIsValid():Boolean
    {
        if (!model_internal::_wijzigingIsValidCacheInitialized)
        {
            model_internal::calculateWijzigingIsValid();
        }

        return model_internal::_wijzigingIsValid;
    }

    model_internal function calculateWijzigingIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_wijzigingValidator.validate(model_internal::_instance.wijziging)
        model_internal::_wijzigingIsValid_der = (valRes.results == null);
        model_internal::_wijzigingIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::wijzigingValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::wijzigingValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get wijzigingValidationFailureMessages():Array
    {
        if (model_internal::_wijzigingValidationFailureMessages == null)
            model_internal::calculateWijzigingIsValid();

        return _wijzigingValidationFailureMessages;
    }

    model_internal function set wijzigingValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_wijzigingValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_wijzigingValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "wijzigingValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get VertrekSpoorStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get VertrekSpoorValidator() : StyleValidator
    {
        return model_internal::_VertrekSpoorValidator;
    }

    model_internal function set _VertrekSpoorIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_VertrekSpoorIsValid;         
        if (oldValue !== value)
        {
            model_internal::_VertrekSpoorIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekSpoorIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get VertrekSpoorIsValid():Boolean
    {
        if (!model_internal::_VertrekSpoorIsValidCacheInitialized)
        {
            model_internal::calculateVertrekSpoorIsValid();
        }

        return model_internal::_VertrekSpoorIsValid;
    }

    model_internal function calculateVertrekSpoorIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_VertrekSpoorValidator.validate(model_internal::_instance.VertrekSpoor)
        model_internal::_VertrekSpoorIsValid_der = (valRes.results == null);
        model_internal::_VertrekSpoorIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::VertrekSpoorValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::VertrekSpoorValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get VertrekSpoorValidationFailureMessages():Array
    {
        if (model_internal::_VertrekSpoorValidationFailureMessages == null)
            model_internal::calculateVertrekSpoorIsValid();

        return _VertrekSpoorValidationFailureMessages;
    }

    model_internal function set VertrekSpoorValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_VertrekSpoorValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_VertrekSpoorValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekSpoorValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            case("wijziging"):
            {
                return wijzigingValidationFailureMessages;
            }
            case("VertrekSpoor"):
            {
                return VertrekSpoorValidationFailureMessages;
            }
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
