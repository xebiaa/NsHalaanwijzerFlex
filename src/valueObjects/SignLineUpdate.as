package valueObjects {
	import components.SignLine;

	public class SignLineUpdate	{
		
		public function SignLineUpdate(newTrainData:VertrekkendeTrein_type, targetSignLine:SignLine) {
			this.newTrainData = newTrainData;
			this.targetSignLine = targetSignLine;
		}
		
		public var newTrainData:VertrekkendeTrein_type;
		public var targetSignLine:SignLine;
	}
}