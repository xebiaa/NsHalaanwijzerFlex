
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.styles.StyleValidator;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.events.ValidationResultEvent;
import valueObjects.VertrekSpoor_type;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _VertrekkendeTrein_typeEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("RitNummer", "VertrekTijd", "VertrekVertraging", "VertrekVertragingTekst", "EindBestemming", "TreinSoort", "RouteTekst", "VertrekSpoor");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array("RitNummer", "VertrekTijd", "VertrekVertraging", "VertrekVertragingTekst", "EindBestemming", "TreinSoort", "RouteTekst", "VertrekSpoor");
    model_internal static var allAlwaysAvailableProperties:Array = new Array("RitNummer", "VertrekTijd", "VertrekVertraging", "VertrekVertragingTekst", "EindBestemming", "TreinSoort", "RouteTekst", "VertrekSpoor");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("RitNummer", "VertrekTijd", "VertrekVertraging", "VertrekVertragingTekst", "EindBestemming", "TreinSoort", "RouteTekst", "VertrekSpoor");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("RitNummer", "VertrekTijd", "VertrekVertraging", "VertrekVertragingTekst", "EindBestemming", "TreinSoort", "RouteTekst", "VertrekSpoor");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "VertrekkendeTrein_type";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;

    
    model_internal var _RitNummerIsValid:Boolean;
    model_internal var _RitNummerValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _RitNummerIsValidCacheInitialized:Boolean = false;
    model_internal var _RitNummerValidationFailureMessages:Array;
    
    model_internal var _VertrekTijdIsValid:Boolean;
    model_internal var _VertrekTijdValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _VertrekTijdIsValidCacheInitialized:Boolean = false;
    model_internal var _VertrekTijdValidationFailureMessages:Array;
    
    model_internal var _VertrekVertragingIsValid:Boolean;
    model_internal var _VertrekVertragingValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _VertrekVertragingIsValidCacheInitialized:Boolean = false;
    model_internal var _VertrekVertragingValidationFailureMessages:Array;
    
    model_internal var _VertrekVertragingTekstIsValid:Boolean;
    model_internal var _VertrekVertragingTekstValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _VertrekVertragingTekstIsValidCacheInitialized:Boolean = false;
    model_internal var _VertrekVertragingTekstValidationFailureMessages:Array;
    
    model_internal var _EindBestemmingIsValid:Boolean;
    model_internal var _EindBestemmingValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _EindBestemmingIsValidCacheInitialized:Boolean = false;
    model_internal var _EindBestemmingValidationFailureMessages:Array;
    
    model_internal var _TreinSoortIsValid:Boolean;
    model_internal var _TreinSoortValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _TreinSoortIsValidCacheInitialized:Boolean = false;
    model_internal var _TreinSoortValidationFailureMessages:Array;
    
    model_internal var _RouteTekstIsValid:Boolean;
    model_internal var _RouteTekstValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _RouteTekstIsValidCacheInitialized:Boolean = false;
    model_internal var _RouteTekstValidationFailureMessages:Array;
    
    model_internal var _VertrekSpoorIsValid:Boolean;
    model_internal var _VertrekSpoorValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _VertrekSpoorIsValidCacheInitialized:Boolean = false;
    model_internal var _VertrekSpoorValidationFailureMessages:Array;

    model_internal var _instance:_Super_VertrekkendeTrein_type;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _VertrekkendeTrein_typeEntityMetadata(value : _Super_VertrekkendeTrein_type)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["RitNummer"] = new Array();
            model_internal::dependentsOnMap["VertrekTijd"] = new Array();
            model_internal::dependentsOnMap["VertrekVertraging"] = new Array();
            model_internal::dependentsOnMap["VertrekVertragingTekst"] = new Array();
            model_internal::dependentsOnMap["EindBestemming"] = new Array();
            model_internal::dependentsOnMap["TreinSoort"] = new Array();
            model_internal::dependentsOnMap["RouteTekst"] = new Array();
            model_internal::dependentsOnMap["VertrekSpoor"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["RitNummer"] = "String";
        model_internal::propertyTypeMap["VertrekTijd"] = "String";
        model_internal::propertyTypeMap["VertrekVertraging"] = "String";
        model_internal::propertyTypeMap["VertrekVertragingTekst"] = "String";
        model_internal::propertyTypeMap["EindBestemming"] = "String";
        model_internal::propertyTypeMap["TreinSoort"] = "String";
        model_internal::propertyTypeMap["RouteTekst"] = "String";
        model_internal::propertyTypeMap["VertrekSpoor"] = "valueObjects.VertrekSpoor_type";

        model_internal::_instance = value;
        model_internal::_RitNummerValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForRitNummer);
        model_internal::_RitNummerValidator.required = true;
        model_internal::_RitNummerValidator.requiredFieldError = "RitNummer is required";
        //model_internal::_RitNummerValidator.source = model_internal::_instance;
        //model_internal::_RitNummerValidator.property = "RitNummer";
        model_internal::_VertrekTijdValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForVertrekTijd);
        model_internal::_VertrekTijdValidator.required = true;
        model_internal::_VertrekTijdValidator.requiredFieldError = "VertrekTijd is required";
        //model_internal::_VertrekTijdValidator.source = model_internal::_instance;
        //model_internal::_VertrekTijdValidator.property = "VertrekTijd";
        model_internal::_VertrekVertragingValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForVertrekVertraging);
        model_internal::_VertrekVertragingValidator.required = true;
        model_internal::_VertrekVertragingValidator.requiredFieldError = "VertrekVertraging is required";
        //model_internal::_VertrekVertragingValidator.source = model_internal::_instance;
        //model_internal::_VertrekVertragingValidator.property = "VertrekVertraging";
        model_internal::_VertrekVertragingTekstValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForVertrekVertragingTekst);
        model_internal::_VertrekVertragingTekstValidator.required = true;
        model_internal::_VertrekVertragingTekstValidator.requiredFieldError = "VertrekVertragingTekst is required";
        //model_internal::_VertrekVertragingTekstValidator.source = model_internal::_instance;
        //model_internal::_VertrekVertragingTekstValidator.property = "VertrekVertragingTekst";
        model_internal::_EindBestemmingValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForEindBestemming);
        model_internal::_EindBestemmingValidator.required = true;
        model_internal::_EindBestemmingValidator.requiredFieldError = "EindBestemming is required";
        //model_internal::_EindBestemmingValidator.source = model_internal::_instance;
        //model_internal::_EindBestemmingValidator.property = "EindBestemming";
        model_internal::_TreinSoortValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForTreinSoort);
        model_internal::_TreinSoortValidator.required = true;
        model_internal::_TreinSoortValidator.requiredFieldError = "TreinSoort is required";
        //model_internal::_TreinSoortValidator.source = model_internal::_instance;
        //model_internal::_TreinSoortValidator.property = "TreinSoort";
        model_internal::_RouteTekstValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForRouteTekst);
        model_internal::_RouteTekstValidator.required = true;
        model_internal::_RouteTekstValidator.requiredFieldError = "RouteTekst is required";
        //model_internal::_RouteTekstValidator.source = model_internal::_instance;
        //model_internal::_RouteTekstValidator.property = "RouteTekst";
        model_internal::_VertrekSpoorValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForVertrekSpoor);
        model_internal::_VertrekSpoorValidator.required = true;
        model_internal::_VertrekSpoorValidator.requiredFieldError = "VertrekSpoor is required";
        //model_internal::_VertrekSpoorValidator.source = model_internal::_instance;
        //model_internal::_VertrekSpoorValidator.property = "VertrekSpoor";
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity VertrekkendeTrein_type");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity VertrekkendeTrein_type");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of VertrekkendeTrein_type");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity VertrekkendeTrein_type");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity VertrekkendeTrein_type");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity VertrekkendeTrein_type");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isRitNummerAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isVertrekTijdAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isVertrekVertragingAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isVertrekVertragingTekstAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEindBestemmingAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTreinSoortAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isRouteTekstAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isVertrekSpoorAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */
    public function invalidateDependentOnRitNummer():void
    {
        if (model_internal::_RitNummerIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfRitNummer = null;
            model_internal::calculateRitNummerIsValid();
        }
    }
    public function invalidateDependentOnVertrekTijd():void
    {
        if (model_internal::_VertrekTijdIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfVertrekTijd = null;
            model_internal::calculateVertrekTijdIsValid();
        }
    }
    public function invalidateDependentOnVertrekVertraging():void
    {
        if (model_internal::_VertrekVertragingIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfVertrekVertraging = null;
            model_internal::calculateVertrekVertragingIsValid();
        }
    }
    public function invalidateDependentOnVertrekVertragingTekst():void
    {
        if (model_internal::_VertrekVertragingTekstIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfVertrekVertragingTekst = null;
            model_internal::calculateVertrekVertragingTekstIsValid();
        }
    }
    public function invalidateDependentOnEindBestemming():void
    {
        if (model_internal::_EindBestemmingIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfEindBestemming = null;
            model_internal::calculateEindBestemmingIsValid();
        }
    }
    public function invalidateDependentOnTreinSoort():void
    {
        if (model_internal::_TreinSoortIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfTreinSoort = null;
            model_internal::calculateTreinSoortIsValid();
        }
    }
    public function invalidateDependentOnRouteTekst():void
    {
        if (model_internal::_RouteTekstIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfRouteTekst = null;
            model_internal::calculateRouteTekstIsValid();
        }
    }
    public function invalidateDependentOnVertrekSpoor():void
    {
        if (model_internal::_VertrekSpoorIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfVertrekSpoor = null;
            model_internal::calculateVertrekSpoorIsValid();
        }
    }

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get RitNummerStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get RitNummerValidator() : StyleValidator
    {
        return model_internal::_RitNummerValidator;
    }

    model_internal function set _RitNummerIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_RitNummerIsValid;         
        if (oldValue !== value)
        {
            model_internal::_RitNummerIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RitNummerIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get RitNummerIsValid():Boolean
    {
        if (!model_internal::_RitNummerIsValidCacheInitialized)
        {
            model_internal::calculateRitNummerIsValid();
        }

        return model_internal::_RitNummerIsValid;
    }

    model_internal function calculateRitNummerIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_RitNummerValidator.validate(model_internal::_instance.RitNummer)
        model_internal::_RitNummerIsValid_der = (valRes.results == null);
        model_internal::_RitNummerIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::RitNummerValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::RitNummerValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get RitNummerValidationFailureMessages():Array
    {
        if (model_internal::_RitNummerValidationFailureMessages == null)
            model_internal::calculateRitNummerIsValid();

        return _RitNummerValidationFailureMessages;
    }

    model_internal function set RitNummerValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_RitNummerValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_RitNummerValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RitNummerValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get VertrekTijdStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get VertrekTijdValidator() : StyleValidator
    {
        return model_internal::_VertrekTijdValidator;
    }

    model_internal function set _VertrekTijdIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_VertrekTijdIsValid;         
        if (oldValue !== value)
        {
            model_internal::_VertrekTijdIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekTijdIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get VertrekTijdIsValid():Boolean
    {
        if (!model_internal::_VertrekTijdIsValidCacheInitialized)
        {
            model_internal::calculateVertrekTijdIsValid();
        }

        return model_internal::_VertrekTijdIsValid;
    }

    model_internal function calculateVertrekTijdIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_VertrekTijdValidator.validate(model_internal::_instance.VertrekTijd)
        model_internal::_VertrekTijdIsValid_der = (valRes.results == null);
        model_internal::_VertrekTijdIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::VertrekTijdValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::VertrekTijdValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get VertrekTijdValidationFailureMessages():Array
    {
        if (model_internal::_VertrekTijdValidationFailureMessages == null)
            model_internal::calculateVertrekTijdIsValid();

        return _VertrekTijdValidationFailureMessages;
    }

    model_internal function set VertrekTijdValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_VertrekTijdValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_VertrekTijdValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekTijdValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get VertrekVertragingStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get VertrekVertragingValidator() : StyleValidator
    {
        return model_internal::_VertrekVertragingValidator;
    }

    model_internal function set _VertrekVertragingIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_VertrekVertragingIsValid;         
        if (oldValue !== value)
        {
            model_internal::_VertrekVertragingIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekVertragingIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get VertrekVertragingIsValid():Boolean
    {
        if (!model_internal::_VertrekVertragingIsValidCacheInitialized)
        {
            model_internal::calculateVertrekVertragingIsValid();
        }

        return model_internal::_VertrekVertragingIsValid;
    }

    model_internal function calculateVertrekVertragingIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_VertrekVertragingValidator.validate(model_internal::_instance.VertrekVertraging)
        model_internal::_VertrekVertragingIsValid_der = (valRes.results == null);
        model_internal::_VertrekVertragingIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::VertrekVertragingValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::VertrekVertragingValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get VertrekVertragingValidationFailureMessages():Array
    {
        if (model_internal::_VertrekVertragingValidationFailureMessages == null)
            model_internal::calculateVertrekVertragingIsValid();

        return _VertrekVertragingValidationFailureMessages;
    }

    model_internal function set VertrekVertragingValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_VertrekVertragingValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_VertrekVertragingValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekVertragingValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get VertrekVertragingTekstStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get VertrekVertragingTekstValidator() : StyleValidator
    {
        return model_internal::_VertrekVertragingTekstValidator;
    }

    model_internal function set _VertrekVertragingTekstIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_VertrekVertragingTekstIsValid;         
        if (oldValue !== value)
        {
            model_internal::_VertrekVertragingTekstIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekVertragingTekstIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get VertrekVertragingTekstIsValid():Boolean
    {
        if (!model_internal::_VertrekVertragingTekstIsValidCacheInitialized)
        {
            model_internal::calculateVertrekVertragingTekstIsValid();
        }

        return model_internal::_VertrekVertragingTekstIsValid;
    }

    model_internal function calculateVertrekVertragingTekstIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_VertrekVertragingTekstValidator.validate(model_internal::_instance.VertrekVertragingTekst)
        model_internal::_VertrekVertragingTekstIsValid_der = (valRes.results == null);
        model_internal::_VertrekVertragingTekstIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::VertrekVertragingTekstValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::VertrekVertragingTekstValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get VertrekVertragingTekstValidationFailureMessages():Array
    {
        if (model_internal::_VertrekVertragingTekstValidationFailureMessages == null)
            model_internal::calculateVertrekVertragingTekstIsValid();

        return _VertrekVertragingTekstValidationFailureMessages;
    }

    model_internal function set VertrekVertragingTekstValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_VertrekVertragingTekstValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_VertrekVertragingTekstValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekVertragingTekstValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get EindBestemmingStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get EindBestemmingValidator() : StyleValidator
    {
        return model_internal::_EindBestemmingValidator;
    }

    model_internal function set _EindBestemmingIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_EindBestemmingIsValid;         
        if (oldValue !== value)
        {
            model_internal::_EindBestemmingIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EindBestemmingIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get EindBestemmingIsValid():Boolean
    {
        if (!model_internal::_EindBestemmingIsValidCacheInitialized)
        {
            model_internal::calculateEindBestemmingIsValid();
        }

        return model_internal::_EindBestemmingIsValid;
    }

    model_internal function calculateEindBestemmingIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_EindBestemmingValidator.validate(model_internal::_instance.EindBestemming)
        model_internal::_EindBestemmingIsValid_der = (valRes.results == null);
        model_internal::_EindBestemmingIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::EindBestemmingValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::EindBestemmingValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get EindBestemmingValidationFailureMessages():Array
    {
        if (model_internal::_EindBestemmingValidationFailureMessages == null)
            model_internal::calculateEindBestemmingIsValid();

        return _EindBestemmingValidationFailureMessages;
    }

    model_internal function set EindBestemmingValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_EindBestemmingValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_EindBestemmingValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EindBestemmingValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get TreinSoortStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get TreinSoortValidator() : StyleValidator
    {
        return model_internal::_TreinSoortValidator;
    }

    model_internal function set _TreinSoortIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_TreinSoortIsValid;         
        if (oldValue !== value)
        {
            model_internal::_TreinSoortIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TreinSoortIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get TreinSoortIsValid():Boolean
    {
        if (!model_internal::_TreinSoortIsValidCacheInitialized)
        {
            model_internal::calculateTreinSoortIsValid();
        }

        return model_internal::_TreinSoortIsValid;
    }

    model_internal function calculateTreinSoortIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_TreinSoortValidator.validate(model_internal::_instance.TreinSoort)
        model_internal::_TreinSoortIsValid_der = (valRes.results == null);
        model_internal::_TreinSoortIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::TreinSoortValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::TreinSoortValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get TreinSoortValidationFailureMessages():Array
    {
        if (model_internal::_TreinSoortValidationFailureMessages == null)
            model_internal::calculateTreinSoortIsValid();

        return _TreinSoortValidationFailureMessages;
    }

    model_internal function set TreinSoortValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_TreinSoortValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_TreinSoortValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TreinSoortValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get RouteTekstStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get RouteTekstValidator() : StyleValidator
    {
        return model_internal::_RouteTekstValidator;
    }

    model_internal function set _RouteTekstIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_RouteTekstIsValid;         
        if (oldValue !== value)
        {
            model_internal::_RouteTekstIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RouteTekstIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get RouteTekstIsValid():Boolean
    {
        if (!model_internal::_RouteTekstIsValidCacheInitialized)
        {
            model_internal::calculateRouteTekstIsValid();
        }

        return model_internal::_RouteTekstIsValid;
    }

    model_internal function calculateRouteTekstIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_RouteTekstValidator.validate(model_internal::_instance.RouteTekst)
        model_internal::_RouteTekstIsValid_der = (valRes.results == null);
        model_internal::_RouteTekstIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::RouteTekstValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::RouteTekstValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get RouteTekstValidationFailureMessages():Array
    {
        if (model_internal::_RouteTekstValidationFailureMessages == null)
            model_internal::calculateRouteTekstIsValid();

        return _RouteTekstValidationFailureMessages;
    }

    model_internal function set RouteTekstValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_RouteTekstValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_RouteTekstValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RouteTekstValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get VertrekSpoorStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get VertrekSpoorValidator() : StyleValidator
    {
        return model_internal::_VertrekSpoorValidator;
    }

    model_internal function set _VertrekSpoorIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_VertrekSpoorIsValid;         
        if (oldValue !== value)
        {
            model_internal::_VertrekSpoorIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekSpoorIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get VertrekSpoorIsValid():Boolean
    {
        if (!model_internal::_VertrekSpoorIsValidCacheInitialized)
        {
            model_internal::calculateVertrekSpoorIsValid();
        }

        return model_internal::_VertrekSpoorIsValid;
    }

    model_internal function calculateVertrekSpoorIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_VertrekSpoorValidator.validate(model_internal::_instance.VertrekSpoor)
        model_internal::_VertrekSpoorIsValid_der = (valRes.results == null);
        model_internal::_VertrekSpoorIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::VertrekSpoorValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::VertrekSpoorValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get VertrekSpoorValidationFailureMessages():Array
    {
        if (model_internal::_VertrekSpoorValidationFailureMessages == null)
            model_internal::calculateVertrekSpoorIsValid();

        return _VertrekSpoorValidationFailureMessages;
    }

    model_internal function set VertrekSpoorValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_VertrekSpoorValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_VertrekSpoorValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekSpoorValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            case("RitNummer"):
            {
                return RitNummerValidationFailureMessages;
            }
            case("VertrekTijd"):
            {
                return VertrekTijdValidationFailureMessages;
            }
            case("VertrekVertraging"):
            {
                return VertrekVertragingValidationFailureMessages;
            }
            case("VertrekVertragingTekst"):
            {
                return VertrekVertragingTekstValidationFailureMessages;
            }
            case("EindBestemming"):
            {
                return EindBestemmingValidationFailureMessages;
            }
            case("TreinSoort"):
            {
                return TreinSoortValidationFailureMessages;
            }
            case("RouteTekst"):
            {
                return RouteTekstValidationFailureMessages;
            }
            case("VertrekSpoor"):
            {
                return VertrekSpoorValidationFailureMessages;
            }
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
