/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - VertrekSpoor_type.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_VertrekSpoor_type extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _VertrekSpoor_typeEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_wijziging : String;
    private var _internal_VertrekSpoor : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_VertrekSpoor_type()
    {
        _model = new _VertrekSpoor_typeEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "wijziging", model_internal::setterListenerWijziging));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "VertrekSpoor", model_internal::setterListenerVertrekSpoor));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get wijziging() : String
    {
        return _internal_wijziging;
    }

    [Bindable(event="propertyChange")]
    public function get VertrekSpoor() : String
    {
        return _internal_VertrekSpoor;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set wijziging(value:String) : void
    {
        var oldValue:String = _internal_wijziging;
        if (oldValue !== value)
        {
            _internal_wijziging = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "wijziging", oldValue, _internal_wijziging));
        }
    }

    public function set VertrekSpoor(value:String) : void
    {
        var oldValue:String = _internal_VertrekSpoor;
        if (oldValue !== value)
        {
            _internal_VertrekSpoor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekSpoor", oldValue, _internal_VertrekSpoor));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerWijziging(value:flash.events.Event):void
    {
        _model.invalidateDependentOnWijziging();
    }

    model_internal function setterListenerVertrekSpoor(value:flash.events.Event):void
    {
        _model.invalidateDependentOnVertrekSpoor();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */
    

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.wijzigingIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_wijzigingValidationFailureMessages);
        }
        if (!_model.VertrekSpoorIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_VertrekSpoorValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _VertrekSpoor_typeEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _VertrekSpoor_typeEntityMetadata) : void
    {
        var oldValue : _VertrekSpoor_typeEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfWijziging : Array = null;
    model_internal var _doValidationLastValOfWijziging : String;

    model_internal function _doValidationForWijziging(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfWijziging != null && model_internal::_doValidationLastValOfWijziging == value)
           return model_internal::_doValidationCacheOfWijziging ;

        _model.model_internal::_wijzigingIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isWijzigingAvailable && _internal_wijziging == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "wijziging is required"));
        }

        model_internal::_doValidationCacheOfWijziging = validationFailures;
        model_internal::_doValidationLastValOfWijziging = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfVertrekSpoor : Array = null;
    model_internal var _doValidationLastValOfVertrekSpoor : String;

    model_internal function _doValidationForVertrekSpoor(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfVertrekSpoor != null && model_internal::_doValidationLastValOfVertrekSpoor == value)
           return model_internal::_doValidationCacheOfVertrekSpoor ;

        _model.model_internal::_VertrekSpoorIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isVertrekSpoorAvailable && _internal_VertrekSpoor == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "VertrekSpoor is required"));
        }

        model_internal::_doValidationCacheOfVertrekSpoor = validationFailures;
        model_internal::_doValidationLastValOfVertrekSpoor = value;

        return validationFailures;
    }
    

}

}
