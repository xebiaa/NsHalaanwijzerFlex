/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - VertrekkendeTrein_type.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;
import valueObjects.VertrekSpoor_type;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_VertrekkendeTrein_type extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.VertrekSpoor_type.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _VertrekkendeTrein_typeEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_RitNummer : String;
    private var _internal_VertrekTijd : String;
    private var _internal_VertrekVertraging : String;
    private var _internal_VertrekVertragingTekst : String;
    private var _internal_EindBestemming : String;
    private var _internal_TreinSoort : String;
    private var _internal_RouteTekst : String;
    private var _internal_VertrekSpoor : valueObjects.VertrekSpoor_type;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_VertrekkendeTrein_type()
    {
        _model = new _VertrekkendeTrein_typeEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "RitNummer", model_internal::setterListenerRitNummer));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "VertrekTijd", model_internal::setterListenerVertrekTijd));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "VertrekVertraging", model_internal::setterListenerVertrekVertraging));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "VertrekVertragingTekst", model_internal::setterListenerVertrekVertragingTekst));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "EindBestemming", model_internal::setterListenerEindBestemming));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "TreinSoort", model_internal::setterListenerTreinSoort));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "RouteTekst", model_internal::setterListenerRouteTekst));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "VertrekSpoor", model_internal::setterListenerVertrekSpoor));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get RitNummer() : String
    {
        return _internal_RitNummer;
    }

    [Bindable(event="propertyChange")]
    public function get VertrekTijd() : String
    {
        return _internal_VertrekTijd;
    }

    [Bindable(event="propertyChange")]
    public function get VertrekVertraging() : String
    {
        return _internal_VertrekVertraging;
    }

    [Bindable(event="propertyChange")]
    public function get VertrekVertragingTekst() : String
    {
        return _internal_VertrekVertragingTekst;
    }

    [Bindable(event="propertyChange")]
    public function get EindBestemming() : String
    {
        return _internal_EindBestemming;
    }

    [Bindable(event="propertyChange")]
    public function get TreinSoort() : String
    {
        return _internal_TreinSoort;
    }

    [Bindable(event="propertyChange")]
    public function get RouteTekst() : String
    {
        return _internal_RouteTekst;
    }

    [Bindable(event="propertyChange")]
    public function get VertrekSpoor() : valueObjects.VertrekSpoor_type
    {
        return _internal_VertrekSpoor;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set RitNummer(value:String) : void
    {
        var oldValue:String = _internal_RitNummer;
        if (oldValue !== value)
        {
            _internal_RitNummer = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RitNummer", oldValue, _internal_RitNummer));
        }
    }

    public function set VertrekTijd(value:String) : void
    {
        var oldValue:String = _internal_VertrekTijd;
        if (oldValue !== value)
        {
            _internal_VertrekTijd = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekTijd", oldValue, _internal_VertrekTijd));
        }
    }

    public function set VertrekVertraging(value:String) : void
    {
        var oldValue:String = _internal_VertrekVertraging;
        if (oldValue !== value)
        {
            _internal_VertrekVertraging = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekVertraging", oldValue, _internal_VertrekVertraging));
        }
    }

    public function set VertrekVertragingTekst(value:String) : void
    {
        var oldValue:String = _internal_VertrekVertragingTekst;
        if (oldValue !== value)
        {
            _internal_VertrekVertragingTekst = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekVertragingTekst", oldValue, _internal_VertrekVertragingTekst));
        }
    }

    public function set EindBestemming(value:String) : void
    {
        var oldValue:String = _internal_EindBestemming;
        if (oldValue !== value)
        {
            _internal_EindBestemming = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EindBestemming", oldValue, _internal_EindBestemming));
        }
    }

    public function set TreinSoort(value:String) : void
    {
        var oldValue:String = _internal_TreinSoort;
        if (oldValue !== value)
        {
            _internal_TreinSoort = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "TreinSoort", oldValue, _internal_TreinSoort));
        }
    }

    public function set RouteTekst(value:String) : void
    {
        var oldValue:String = _internal_RouteTekst;
        if (oldValue !== value)
        {
            _internal_RouteTekst = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "RouteTekst", oldValue, _internal_RouteTekst));
        }
    }

    public function set VertrekSpoor(value:valueObjects.VertrekSpoor_type) : void
    {
        var oldValue:valueObjects.VertrekSpoor_type = _internal_VertrekSpoor;
        if (oldValue !== value)
        {
            _internal_VertrekSpoor = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "VertrekSpoor", oldValue, _internal_VertrekSpoor));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerRitNummer(value:flash.events.Event):void
    {
        _model.invalidateDependentOnRitNummer();
    }

    model_internal function setterListenerVertrekTijd(value:flash.events.Event):void
    {
        _model.invalidateDependentOnVertrekTijd();
    }

    model_internal function setterListenerVertrekVertraging(value:flash.events.Event):void
    {
        _model.invalidateDependentOnVertrekVertraging();
    }

    model_internal function setterListenerVertrekVertragingTekst(value:flash.events.Event):void
    {
        _model.invalidateDependentOnVertrekVertragingTekst();
    }

    model_internal function setterListenerEindBestemming(value:flash.events.Event):void
    {
        _model.invalidateDependentOnEindBestemming();
    }

    model_internal function setterListenerTreinSoort(value:flash.events.Event):void
    {
        _model.invalidateDependentOnTreinSoort();
    }

    model_internal function setterListenerRouteTekst(value:flash.events.Event):void
    {
        _model.invalidateDependentOnRouteTekst();
    }

    model_internal function setterListenerVertrekSpoor(value:flash.events.Event):void
    {
        _model.invalidateDependentOnVertrekSpoor();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */
    

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.RitNummerIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_RitNummerValidationFailureMessages);
        }
        if (!_model.VertrekTijdIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_VertrekTijdValidationFailureMessages);
        }
        if (!_model.VertrekVertragingIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_VertrekVertragingValidationFailureMessages);
        }
        if (!_model.VertrekVertragingTekstIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_VertrekVertragingTekstValidationFailureMessages);
        }
        if (!_model.EindBestemmingIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_EindBestemmingValidationFailureMessages);
        }
        if (!_model.TreinSoortIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_TreinSoortValidationFailureMessages);
        }
        if (!_model.RouteTekstIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_RouteTekstValidationFailureMessages);
        }
        if (!_model.VertrekSpoorIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_VertrekSpoorValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _VertrekkendeTrein_typeEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _VertrekkendeTrein_typeEntityMetadata) : void
    {
        var oldValue : _VertrekkendeTrein_typeEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfRitNummer : Array = null;
    model_internal var _doValidationLastValOfRitNummer : String;

    model_internal function _doValidationForRitNummer(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfRitNummer != null && model_internal::_doValidationLastValOfRitNummer == value)
           return model_internal::_doValidationCacheOfRitNummer ;

        _model.model_internal::_RitNummerIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isRitNummerAvailable && _internal_RitNummer == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "RitNummer is required"));
        }

        model_internal::_doValidationCacheOfRitNummer = validationFailures;
        model_internal::_doValidationLastValOfRitNummer = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfVertrekTijd : Array = null;
    model_internal var _doValidationLastValOfVertrekTijd : String;

    model_internal function _doValidationForVertrekTijd(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfVertrekTijd != null && model_internal::_doValidationLastValOfVertrekTijd == value)
           return model_internal::_doValidationCacheOfVertrekTijd ;

        _model.model_internal::_VertrekTijdIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isVertrekTijdAvailable && _internal_VertrekTijd == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "VertrekTijd is required"));
        }

        model_internal::_doValidationCacheOfVertrekTijd = validationFailures;
        model_internal::_doValidationLastValOfVertrekTijd = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfVertrekVertraging : Array = null;
    model_internal var _doValidationLastValOfVertrekVertraging : String;

    model_internal function _doValidationForVertrekVertraging(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfVertrekVertraging != null && model_internal::_doValidationLastValOfVertrekVertraging == value)
           return model_internal::_doValidationCacheOfVertrekVertraging ;

        _model.model_internal::_VertrekVertragingIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isVertrekVertragingAvailable && _internal_VertrekVertraging == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "VertrekVertraging is required"));
        }

        model_internal::_doValidationCacheOfVertrekVertraging = validationFailures;
        model_internal::_doValidationLastValOfVertrekVertraging = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfVertrekVertragingTekst : Array = null;
    model_internal var _doValidationLastValOfVertrekVertragingTekst : String;

    model_internal function _doValidationForVertrekVertragingTekst(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfVertrekVertragingTekst != null && model_internal::_doValidationLastValOfVertrekVertragingTekst == value)
           return model_internal::_doValidationCacheOfVertrekVertragingTekst ;

        _model.model_internal::_VertrekVertragingTekstIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isVertrekVertragingTekstAvailable && _internal_VertrekVertragingTekst == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "VertrekVertragingTekst is required"));
        }

        model_internal::_doValidationCacheOfVertrekVertragingTekst = validationFailures;
        model_internal::_doValidationLastValOfVertrekVertragingTekst = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfEindBestemming : Array = null;
    model_internal var _doValidationLastValOfEindBestemming : String;

    model_internal function _doValidationForEindBestemming(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfEindBestemming != null && model_internal::_doValidationLastValOfEindBestemming == value)
           return model_internal::_doValidationCacheOfEindBestemming ;

        _model.model_internal::_EindBestemmingIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isEindBestemmingAvailable && _internal_EindBestemming == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "EindBestemming is required"));
        }

        model_internal::_doValidationCacheOfEindBestemming = validationFailures;
        model_internal::_doValidationLastValOfEindBestemming = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfTreinSoort : Array = null;
    model_internal var _doValidationLastValOfTreinSoort : String;

    model_internal function _doValidationForTreinSoort(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfTreinSoort != null && model_internal::_doValidationLastValOfTreinSoort == value)
           return model_internal::_doValidationCacheOfTreinSoort ;

        _model.model_internal::_TreinSoortIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isTreinSoortAvailable && _internal_TreinSoort == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "TreinSoort is required"));
        }

        model_internal::_doValidationCacheOfTreinSoort = validationFailures;
        model_internal::_doValidationLastValOfTreinSoort = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfRouteTekst : Array = null;
    model_internal var _doValidationLastValOfRouteTekst : String;

    model_internal function _doValidationForRouteTekst(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfRouteTekst != null && model_internal::_doValidationLastValOfRouteTekst == value)
           return model_internal::_doValidationCacheOfRouteTekst ;

        _model.model_internal::_RouteTekstIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isRouteTekstAvailable && _internal_RouteTekst == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "RouteTekst is required"));
        }

        model_internal::_doValidationCacheOfRouteTekst = validationFailures;
        model_internal::_doValidationLastValOfRouteTekst = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfVertrekSpoor : Array = null;
    model_internal var _doValidationLastValOfVertrekSpoor : valueObjects.VertrekSpoor_type;

    model_internal function _doValidationForVertrekSpoor(valueIn:Object):Array
    {
        var value : valueObjects.VertrekSpoor_type = valueIn as valueObjects.VertrekSpoor_type;

        if (model_internal::_doValidationCacheOfVertrekSpoor != null && model_internal::_doValidationLastValOfVertrekSpoor == value)
           return model_internal::_doValidationCacheOfVertrekSpoor ;

        _model.model_internal::_VertrekSpoorIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isVertrekSpoorAvailable && _internal_VertrekSpoor == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "VertrekSpoor is required"));
        }

        model_internal::_doValidationCacheOfVertrekSpoor = validationFailures;
        model_internal::_doValidationLastValOfVertrekSpoor = value;

        return validationFailures;
    }
    

}

}
