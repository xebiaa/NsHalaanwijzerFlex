package events
{
	import flash.events.Event;
	
	public class FlippingEvent extends Event
	{
		
		public static const FLIPPER_FINISHED:String = "flipperFinished";
		public static const LINE_FINISHED:String = "lineFinished";
		
		public function FlippingEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}