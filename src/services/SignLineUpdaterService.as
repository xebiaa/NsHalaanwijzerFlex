package services
{
	import appmodel.ModelLocator;
	
	import components.SignLine;
	
	import events.FlippingEvent;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.CallResponder;
	import mx.rpc.events.ResultEvent;
	
	import services.nsapi.NsApi;
	
	import valueObjects.SignLineUpdate;
	import valueObjects.VertrekkendeTrein_type;

	public class SignLineUpdaterService
	{
		private var model:ModelLocator = ModelLocator.getInstance();
		private const MAX_LINES_FLIPPING:int = 4;
		private var currentLinesFlipping:int = 0;
		private var nsApi:NsApi = new NsApi();
		private var getDataResponder:CallResponder = new CallResponder();
		private var timer:Timer = new Timer(model.NS_API_CALL_DELAY);
		
		public function SignLineUpdaterService() {
			getDataResponder.addEventListener(ResultEvent.RESULT,getData_resultHandler);
			doGetData(null);
			timer.repeatCount = 0;
			timer.addEventListener(TimerEvent.TIMER, doGetData);
			timer.start();
		}
		
		protected function doGetData(event:TimerEvent):void
		{
			getDataResponder.token = nsApi.getData("ut");
		}
		
		protected function getData_resultHandler(event:ResultEvent):void {
			trace("Got data from NS API");
			var result:ArrayCollection = event.result as ArrayCollection;
			updateReels(result);
			processNsApiData(result);
			
		}
		
		private function updateReels(data:ArrayCollection):void {
			for each (var train:VertrekkendeTrein_type in data) {
				if (!model.destinationReel.contains(train.EindBestemming)) {
					model.destinationReel.addItem(train.EindBestemming);
				}
				if (!model.trainTypeReel.contains(train.TreinSoort)) {
					model.trainTypeReel.addItem(train.TreinSoort);
				}
				if (train.VertrekVertragingTekst != null && !model.delayReel.contains(train.VertrekVertragingTekst)) {
					model.delayReel.addItem(train.VertrekVertragingTekst);
				}
				if (!model.trackReel.contains(train.VertrekSpoor.VertrekSpoor)) {
					model.trackReel.addItem(train.VertrekSpoor.VertrekSpoor);
				}
			}
			model.destinationReel.source.sort();
			model.destinationReel.refresh();
			model.trainTypeReel.source.sort();
			model.trainTypeReel.refresh();
			model.delayReel.source.sort();
			model.delayReel.refresh();
			model.trackReel.source.sort();
			model.trackReel.refresh();
		}
		
		public function processNsApiData(data:ArrayCollection):void {
			var i:int = 0;
			for each (var train:VertrekkendeTrein_type in data) {
				if(i >= model.signLines.length) break;
				var signLine:SignLine = model.signLines[i++] as SignLine;
				model.signUpdateQueue.push(new SignLineUpdate(train, signLine));
			}
			startFlipping();
		}
		
		private function startFlipping():void {
			//trace("StartFlipping. currentLinesFlipping:" + currentLinesFlipping + " queue:" + model.signUpdateQueue.length);
			if (currentLinesFlipping > 0) return;
			for (var i:int = 0 ; i < MAX_LINES_FLIPPING ; i++) {
				if( model.signUpdateQueue.length == 0) break;
				//trace("Starting new line flipping");
				currentLinesFlipping ++;
				var update:SignLineUpdate = model.signUpdateQueue.shift() as SignLineUpdate;
				update.targetSignLine.traindata = update.newTrainData;
			}
		}
		
		public function flippingFinished(event:FlippingEvent):void {
			//trace("Line flipping stopped");
			currentLinesFlipping--;
			startFlipping();
		}
				
	}
}