/**
 * This is a generated sub-class of _NsApi.as and is intended for behavior
 * customization.  This class is only generated when there is no file already present
 * at its target location.  Thus custom behavior that you add here will survive regeneration
 * of the super-class. 
 **/
 
package services.nsapi
{
	import mx.rpc.http.AbstractOperation;
	import mx.utils.Base64Encoder;

public class NsApi extends _Super_NsApi
{
    /**
     * Override super.init() to provide any initialization customization if needed.
     */
    protected override function preInitializeService():void
    {
        super.preInitializeService();
        // Initialization customization goes here
		var operation:AbstractOperation = _serviceControl.getOperation("getData") as AbstractOperation;
		var encoder:Base64Encoder = new Base64Encoder();
		encoder.insertNewLines = false; // prevents the auto inserting of newlines wrecking your header
		encoder.encode("ios:iOS2011");
		operation.headers = {Authorization:"Basic " + encoder.toString()};                                                
		
    }
}

}
